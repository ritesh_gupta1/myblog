<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/blog/{id}', 'HomeController@getBlogDetails');
Route::get('/admin', 'AuthController@admin');
Route::post('/addblog', 'AuthController@addblog')->name('addblog');
Route::post('/editblog', 'AuthController@editblog')->name('editblog');
Route::get('/delete_blog/{id}', 'AuthController@deleteBlog');
Route::get('/togglepublish/{id}/{mode}', 'AuthController@togglepublish');
Route::post('/subscribe', 'HomeController@subscribe')->name('subscribe');
Route::post('/fileupload', 'HomeController@uploadfile')->name('fileupload');
Route::get('/filelist', 'HomeController@filelist')->name('filelist');


Auth::routes();

