<?php

namespace App\Http\Controllers;

use App\Models\Post;
use View;
use Illuminate\Http\Request;
use App\Models\Subscriber;
use Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    

    public function index()
    {
        $bloglist = Post::orderBy('posts.id', 'DESC')->join('users', 'users.id', '=', 'posts.user_id')->where('publish','=','1')->where('status','=','0')->paginate(10);
        return View::make('welcome', ['data' => $bloglist]);
    }


    public function getBlogDetails($id)
    {
        $blog = Post::find($id);
        return View::make('blogdetail', ['blog' => $blog]);
    }
    

    public function subscribe(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            "email" => "required|email",
            "user_id" => "required"
        ]);
        if ($validator->passes()) {
            $subscribe = new Subscriber;
            $subscribe->user_id = $data['user_id'];
            $subscribe->email = $data['email'];
            $subscribe->save();
            return redirect('/');
        }
    }


    public function uploadfile(Request $request)
    {
        $file = $request->file('file');
        $file->move(public_path().'/upload', $file->getClientOriginalName());
        return redirect('filelist');
        
    }
    public function filelist()
    {
        $path = public_path('upload');
        $files = scandir($path);
        unset($files[0]);
        unset($files[1]);
        return View::make('file',['files'=> $files]);
    }


    
}
