<?php

namespace App\Http\Controllers;

use App\Models\Post;
use View;
use Validator;
use Auth;
use App\Models\Subscriber;
use Mail;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   
    public function admin()
    {
        $user_id = Auth::id();
        $bloglist = Post::orderBy('id', 'DESC')->where('user_id', '=', $user_id)->where('status','=','0')->paginate(10);
        return View::make('adminpanel',['data'=> $bloglist]);
    }


    public function addblog(Request $request){
        $data=$request->all();
        $validator=Validator::make($data, [
            "title" => "required",
            "body" => "required"
        ]);
        if ($validator->passes()) {
            $user_id=Auth::id();
            $post=new Post;
            $post->title=$data['title'];
            $post->body = $data['body'];
            $post->user_id = $user_id;
            $post->save();
            return redirect('admin');
        }

    }
        

    public function editblog(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            "title" => "required",
            "body" => "required",
            "id" => "required|numeric"
        ]);
        if ($validator->passes()) {
            $post=Post::find($data['id']);
            $post->title=$data['title'];
            $post->body = $data['body'];
            $post->save();
            return redirect('admin');
        }
    }


    public function deleteBlog( $id)
    {
        $validator = Validator::make(array('id'=>$id), [
            "id" => "required|numeric"
        ]);
        if ($validator->passes()) {
            $post = Post::find($id);
            $post->status = 1;
            $post->save();
            return redirect('admin');
        }
    }

    public function togglepublish($id,$mode)
    {
        $validator = Validator::make(array('id' => $id,'mode'=>$mode), [
            "id" => "required|numeric",
            "mode" => "required|numeric"
        ]);
        if ($validator->passes()) {
            $post = Post::find($id);
            $post->publish = $mode;
            $post->save();
            if($mode == 1){
                $blog = Post::find($id)->toarray();
                    $this->sendMail($blog['title'],$id);
            }

            return redirect('admin');
        }
    }

    public function sendMail($title,$id)
    {   
        $user_id = Auth::id();
        $user_name = Auth::user()->name;
        $subscriberlist = Subscriber::orderBy('id', 'ASC')->where('user_id', '=', $user_id)->pluck('email');
        $data = array('title' => $title, 'user_name' => $user_name,'id'=>$id);
       $from_mail= config('config.FROM_EMAIL');
        foreach ($subscriberlist as $key => $email) {
            Mail::send('emails.reminder', ['data'=>$data], function ($message) use ($email, $from_mail,$data) {
                $message->to($email)->subject($data['title']);
                $message->from($from_mail, 'MYBLOG');
            });
        }
        return redirect('admin');
    }


}
