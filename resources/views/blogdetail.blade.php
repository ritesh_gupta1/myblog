@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>{{ $blog->title }}</h1></div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <span>Submitted in {{ date("F j, Y, g:i a", strtotime($blog->created_at)) }}</span>
                            <p class="lead">{{ $blog->body }}</p>
                        </div>
                    </div>
                </div>






            </div>
        </div>
    </div>
</div>
@endsection