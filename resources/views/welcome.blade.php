@extends('layouts.app')

@section('content')
<div id="subscribe" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ADD NEW BLOG</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('subscribe') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <input id="user_id" type="hidden" name='user_id'>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('ADD') }}
                            </button>
                        </div>
                    </div>
                </form>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Blogs') }}</div>
                <div class="container">
                    <div class="col-md-12">
                        @if(isset($data))
                        @foreach($data as $blog)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <a href="/blog/{{ $blog->id }}">
                                    <h5>{{$blog->name}}</h5>
                                    <h1>{{{ $blog->title }}}</h1>

                                </a>
                                <span>Submitted in {{ date("F j, Y, g:i a", strtotime($blog->created_at)) }}</span>
                            </div>
                            <div class="panel-body">
                                <li class="list-inline-item">
                                    <button class="btn btn-primary btn-sm rounded-0" type="button" id='{{$blog->id}}' data-user_id="{{ $blog->user_id}}" data-placement="top" id="Add" data-toggle="modal" onclick="subscribe({{$blog->id}})" data-target="#subscribe">Subscribe</button>
                                </li>
                                <hr>
                                <!-- </a> -->
                            </div>
                        </div>

                        @endforeach()
                        <div class="pull-right">{{ $data->links() }}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function subscribe(id) {
        var user_id = $('#' + id).data('user_id');
        $('#user_id').val(user_id)
    }
</script>