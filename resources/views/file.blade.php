@extends('layouts.app')

@section('content')
<div id="uploadfile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPLOAD FILE</h4>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __(' File') }}</label>

                    <div class="col-md-6">
                        <input id="file" type="file" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>


                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button id="submit" onclick="submit()" class="btn btn-primary">
                            {{ __('UPLOAD') }}
                        </button>
                    </div>
                </div>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



<div class="container">
    <button class="btn btn-primary btn-sm rounded-0" type="button" data-placement="top" id="Add" data-toggle="modal" data-target="#uploadfile">UPLOAD FILE</button>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Blogs') }}</div>
                <div class="container">
                    <div class="col-md-12">
                        @if(isset($files))
                        @foreach($files as $file)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h5>{{$file}}</h5>
                            </div>
                            <div class="panel-body">
                                <li class="list-inline-item">
                                </li>
                                <hr>
                                <!-- </a> -->
                            </div>
                        </div>

                        @endforeach()
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
    function submit() {
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        fd.append('file', files);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/fileupload',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                location.reload();
            },
        });

    }
</script>