@extends('layouts.app')
@section('content')


<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">EDIT BLOG</h4>
            </div>
            <div class="modal-body">

                <form method="POST" action="{{ route('editblog') }}">
                    @csrf
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Title</label>
                        <div class="col-md-6">
                            <input id="edit_id" type="hidden" name='id'>
                            <input id="edit_title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">Body</label>
                        <div class="col-md-6">
                            <input id="edit_body" type="text" class="form-control @error('body') is-invalid @enderror" name="body" value="{{ old('body') }}" required autocomplete="body" autofocus>
                            @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('ADD') }}
                            </button>
                        </div>
                    </div>
                </form>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ADD NEW BLOG</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('addblog') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Title</label>
                        <div class="col-md-6">
                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Body</label>
                        <div class="col-md-6">
                            <input id="body" type="text" class="form-control @error('body') is-invalid @enderror" name="body" value="{{ old('body') }}" required autocomplete="body" autofocus>
                            @error('body')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('ADD') }}
                            </button>
                        </div>
                    </div>
                </form>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div>
        <button class="btn btn-primary btn-sm rounded-0" type="button" data-placement="top" id="Add" data-toggle="modal" data-target="#addModal">Add</button>

    </div>
    <div class="row justify-content-center">
        <table class="table table-bordered" id="category-table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>detail</th>
                    <th>Created At</th>
                    <th>Action</th>

                </tr>
                @foreach($data as $blog)
                <tr>
                    <td>{{{ $blog->title }}}</td>
                    <td>{{{ $blog->body }}}</td>
                    <td>{{ date("F j, Y, g:i a", strtotime($blog->created_at)) }}</td>
                    <td>
                        <ul class="list-inline m-0">

                            @if($blog->publish == 0)
                            <a href="/togglepublish/{{ $blog->id }}/1">
                                <li class="list-inline-item">
                                    <button class="btn btn-info btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" data-id="{{ $blog->id }}" data-body="{{ $blog->body }}" data-title="{{ $blog->title }}" id="{{ $blog->id }}" name="publish" title="publish">Publish</button>
                                </li>
                            </a>
                            @else
                            <a href="/togglepublish/{{ $blog->id }}/0">
                                <li class="list-inline-item">
                                    <button class="btn btn-warning btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" data-id="{{ $blog->id }}" data-body="{{ $blog->body }}" data-title="{{ $blog->title }}" id="{{ $blog->id }}" name="Unpublish" title="Edit">Unpublish</button>
                                </li>
                            </a>
                            @endif
                            <li class="list-inline-item">
                                <button class="btn btn-success btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" data-id="{{ $blog->id }}" data-body="{{ $blog->body }}" data-title="{{ $blog->title }}" id="{{ $blog->id }}" name="Edit" onclick="edit({{ $blog->id }} )" title="Edit">Edit</button>
                            </li>
                            <li class="list-inline-item">
                                <a href="/delete_blog/{{ $blog->id }}"><button class="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" id="{{ $blog->id }}" value="{{ $blog->id }}" name="Delete" title="Delete">Delete</button></a>
                            </li>
                        </ul>
                    </td>
    </div>
</div>
@endforeach()
</thead>
</table>
@stop
<script>
    function edit(id) {
        var blog_id = $('#' + id).data('id');
        var title = $('#' + id).data('title');
        var body = $('#' + id).data('body');
        $('#edit_id').val(blog_id)
        $('#edit_title').val(title)
        $('#edit_body').val(body)
        $('#editModal').modal('show')
    }
</script>